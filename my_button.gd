extends Button

class_name MyButton
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var bt_dat = 0
var bt_is_num = true

func bt_get_data():
	return {"dat": bt_dat, "is_num": bt_is_num}
	pass
func bt_set_data(dat, is_num):
	bt_dat = dat
	bt_is_num = is_num
	pass
func enPressed():
	BusiLogic.buttonEnPress(bt_get_data())
	pass
# Called when the node enters the scene tree for the first time.
func _ready():
	
	connect("pressed", self, "enPressed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
