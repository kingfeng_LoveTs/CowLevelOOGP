extends Node

class_name MyMath

var num1 = MyNumber.new()
var symb = 0

func comp(form):
	var tNum = MyNumber.new()
	if num1.can_operation(form[0]):
		tNum = num1.operation(form[0], symb, tNum)
	else:
		tNum = form[0].operation(num1, symb, tNum)
		if symb == BusiLogic.BT["SUB"]:
			tNum.do_mins()
	num1 = tNum
	symb = form[1]
	
	print ([num1.get_number(), symb])
	return tNum
	pass

func clear():
	num1 = MyNumber.new()
	symb = BusiLogic.BT["ADD"]
	pass
func init(form):
	num1 = num1.set_number(form[0])
	symb = form[1]
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
