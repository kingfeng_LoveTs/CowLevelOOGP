extends Node

class_name MyNumber
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var number = AbsBaseNumber.new()


func push_number(num):
	if number.can_push(num) :
		number.push_number(num)
	elif AbsFracNumber.new().can_push(num):
		var tN = AbsFracNumber.new()
		tN.upgread(number)
		number = tN
	pass
func get_data():
	return number
	pass

func get_number():
	return number.get_number()
func set_number(num):
	number = num
	pass

func do_mins():
	number.mins()
	pass

func can_operation(num):
	return BusiLogic.NM[number.get_class()] >= BusiLogic.NM[num.get_data().get_class()]
	pass
func operation(num, type, ans):
	var tNum = number
	if type == BusiLogic.BT["ADD"]:
		number.do_add(num.get_data(), tNum)
		ans.set_number(tNum)
	elif type == BusiLogic.BT["SUB"]:
		number.do_sub(num.get_data(), tNum)
		ans.set_number(tNum)
	elif type == BusiLogic.BT["EQU"]:
		ans = self
	else : 
		ans.set_number( tNum.set_number(0) )
	return ans
	pass
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.