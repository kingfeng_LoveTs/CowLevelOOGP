extends Object
class_name AbsBaseNumber



var number = 0
var nClass = "AbsBaseNumber"

func get_class():
	return nClass
	pass

func set_number(num):
	number = num
	pass
func can_push(num):
	return (num is int)
	pass
func push_number(num):
	number *= 10
	number += num
	pass

func mins():
	number = -number
	pass

func get_data():
	return number
	pass
func get_number():
	return str(number)
	pass

func do_add(num, ans):
	ans.set_number(number + num.get_data())
	return ans
	pass 
func do_sub(num, ans):
	ans.set_number(number - num.get_data())
	return ans
	pass