extends AbsBaseNumber
class_name AbsFracNumber



func _init():
	number = [0, 0]
	nClass = "AbsFracNumber"
	pass

var pushMode = true

func get_class():
	return nClass
	pass

func set_number(num):
	number = num
	pass
func can_push(num):
	return ((num is int) or (num == '|'))
	pass
func push_number(num):
	if num is int:
		if pushMode :
			number[0]*=10
			number[0]+=num
		else :
			number[1]*=10
			number[1]+=num
	else:
		pushMode = !pushMode
	pass

func upgread(num):
	if num.get_class() == "AbsBaseNumber":
		number[0] = num.get_data()
	pushMode = false
	pass
func mins():
	number[0] = -number[0]
	pass

func get_data():
	return number
	pass
func get_number():
	return (str(number[0])+"|"+str(number[1]))
	pass

func redu():
	var redN
	redN = number[1] if number[0]>number[1] else number[0]
	while(														\
		(number[0] %redN != 0) or (number[0] %redN != 0)		\
	):
		redN -= 1
		
	if redN!=1 :
		number[0] /=redN
		number[1] /=redN
	pass

func do_add(num, ans):
	var numD = num.get_data()
	if num.get_class() == "AbsBaseNumber":
		ans.set_number([number[0] + numD*number[1], number[1]])
		ans.redu()
	else:
		ans.set_number([	number[0]*numD[1]+number[1]*numD[0]	\
			,					number[1]*numD[1]				])
		ans.redu()
	return ans
	pass 
func do_sub(num, ans):
	var numD = num.get_data()
	if num.get_class() == "AbsBaseNumber":
		ans.set_number([number[0] - numD*number[1], number[1]])
		ans.redu()
	else:
		ans.set_number([	number[0]*numD[1]-number[1]*numD[0]	\
			,					number[1]*numD[1]				])
		ans.redu()
	return ans
	pass