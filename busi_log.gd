extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const BT = {
	"ADD": 0,
	"SUB": 1,
	"EQU": 2,
	"CLR": 3
}
const BT_TEXT = ["+", "-", "=", "c"]

const NM = {
	"AbsBaseNumber" : 0,
	"AbsFracNumber" : 1
	}
# Called when the node enters the scene tree for the first time.
var math_str = "0"
var math_arr = []
var tNum = MyNumber.new()

var en_equ = false

var math = MyMath.new()

func buttonEnPress(btn):
	if btn["is_num"]:
		if en_equ :
			math.clear()
			en_equ = false
		tNum.push_number(btn["dat"])
		math_str = tNum.get_number()
	else:
		if btn["dat"] == BT["CLR"]:
			math.clear()
			math_str = "0"
		else :
			en_equ = false
			math_str = math.comp([tNum, btn["dat"]]).get_number()
			tNum = MyNumber.new()
			if btn["dat"] == BT["EQU"]: en_equ = true
	pass
func get_str():
	return math_str
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
