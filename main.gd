extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const LOC = [32, 1, 2, 3, 11, 12, 13, 21, 22, 23, 4, 
				14, 24, 34, 44, 5]
const BTH = 50
const BTW = 140

var buttons = []
# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(10):
		createButton(i,0 , true, str(i))
	for i in BusiLogic.BT_TEXT.size():
		createButton(i,10 , false, BusiLogic.BT_TEXT[i])
	createButton('|', 15, true, '|')
	pass # Replace with function body.

func createButton(i, loc, is_num, mask):
	var tBut
	tBut = MyButton.new()
	tBut.set_size(Vector2(BTW, BTH) )
	tBut.bt_set_data(i, is_num)
	i = i if (i is int) else 0
	tBut.set_position(Vector2( LOC[i+loc]%10*BTW, LOC[i+loc]/10*BTH + 60))
	tBut.set_text(mask)
	add_child(tBut)
	buttons.push_back(tBut)
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
